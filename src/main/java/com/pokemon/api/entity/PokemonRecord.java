package com.pokemon.api.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="pokemon_record")
@Table(name="\"pokemon_records\"", schema = "public")
public class PokemonRecord {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "user_id")
	private int userId;

	@Column(name = "pokemon_id")
	private int pokemonId;

	@Column(name = "seen")
	private int seen;

	@Column(name = "captured")
	private int captured;

	@Column(name = "skiped")
	private int skiped;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getPokemonId() {
		return pokemonId;
	}

	public void setPokemonId(int pokemonId) {
		this.pokemonId = pokemonId;
	}

	public int getSeen() {
		return seen;
	}

	public void setSeen(int seen) {
		this.seen = seen;
	}

	public int getCaptured() {
		return captured;
	}

	public void setCaptured(int captured) {
		this.captured = captured;
	}

	public int getSkiped() {
		return skiped;
	}

	public void setSkiped(int skiped) {
		this.skiped = skiped;
	}
	
}
