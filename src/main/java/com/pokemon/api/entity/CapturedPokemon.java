package com.pokemon.api.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="captured_pokemon")
@Table(name="\"captured_pokemons\"", schema = "public")
public class CapturedPokemon {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "user_id")
	private int userId;

	@Column(name = "pokemon_id")
	private int pokemonId;

	@Column(name = "defense")
	private int defense;

	@Column(name = "attack")
	private int attack;

	@Column(name = "defense_desc")
	private String defenseDesc;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getPokemonId() {
		return pokemonId;
	}

	public void setPokemonId(int pokemonId) {
		this.pokemonId = pokemonId;
	}

	public int getDefense() {
		return defense;
	}

	public void setDefense(int defense) {
		this.defense = defense;
	}

	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}

	public String getDefenseDesc() {
		return defenseDesc;
	}

	public void setDefenseDesc(String defenseDesc) {
		this.defenseDesc = defenseDesc;
	}

}
