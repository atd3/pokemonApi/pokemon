package com.pokemon.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pokemon.api.entity.User;
import com.pokemon.api.pojos.Resultado;
import com.pokemon.api.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;
	
	@PostMapping
	public User saveUser(@RequestBody User user) {
		return userService.insertUser(user);
	}

	/**
	 * Para esta version se tiene este metodo de login básico, lo ideal es regresar un token para pasarlo a todos endpoint
	 * @param user
	 * @return objeto resultado
	 */
	@PostMapping(value = "/login")
	public Resultado login(@RequestBody User user) {
		return userService.login(user);
	}

	@GetMapping(value = "/data/{userName}")
	public User getUser(@PathVariable String userName) {
		return userService.getUser(userName);
	}

}
