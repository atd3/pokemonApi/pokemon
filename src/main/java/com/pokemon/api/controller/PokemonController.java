package com.pokemon.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pokemon.api.entity.CapturedPokemon;
import com.pokemon.api.entity.Pokemon;
import com.pokemon.api.entity.PokemonRecord;
import com.pokemon.api.entity.TargetPokemon;
import com.pokemon.api.pojos.Resultado;
import com.pokemon.api.pojos.ResultadoBusqueda;
import com.pokemon.api.service.PokemonService;

@RestController
@RequestMapping("/pokemones")
public class PokemonController {

	@Autowired
	private PokemonService pokemonService;
	
	@GetMapping(value = "/busqueda")
	public ResultadoBusqueda buscarPokemon() {
		return pokemonService.buscarPokemonEnApi();
	}
	
	@DeleteMapping(value = "/escape")
	public Resultado escaparPokemon(@RequestBody TargetPokemon targetPokemon) {
		Resultado resultado = new Resultado();
		resultado.setResultado(pokemonService.escaparPokemon(targetPokemon.getId()));
		return resultado;
	}
	
	@PostMapping(value = "/capturados")
	public CapturedPokemon capturarPokemon(@RequestBody Pokemon pokemon) {
		return pokemonService.capturarPokemon(pokemon, 1);
	}

	@GetMapping(value = "/registros/{id}")
	public List<PokemonRecord> getRecord(@PathVariable int id) {
		return pokemonService.getPokemonRecordByUser(id);
	}

	@GetMapping(value = "/capturados/{id}")
	public List<CapturedPokemon> getCaptured(@PathVariable int id) {
		return pokemonService.getPokemonCapturedByUser(id);
	}

	@DeleteMapping(value = "/liberados")
	public Resultado liberarPokemon(@RequestBody CapturedPokemon capturedPokemon) {
		Resultado resultado = new Resultado();
		resultado.setResultado(pokemonService.liberarPokemon( capturedPokemon.getId() ));
		return resultado;
	}
	
	
}
