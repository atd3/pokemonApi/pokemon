package com.pokemon.api.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	/**
	 * Se configura que permita el acceso a todas las url que incluyan user y sean de tipo POST
	 * Para las url que tengan pokemones en el contexto se solicitarán credenciales básicas, para POST, GET y DELETE
	 * Esta versión solo tiene autenticación básica, con Username: ojimarez y Password: ojimarez123, indicadas en application.properties
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.httpBasic()
			.and()
			.authorizeRequests()
			.antMatchers(HttpMethod.POST, "/user/**").permitAll()
			.antMatchers(HttpMethod.POST, "/pokemones/**").authenticated()
			.antMatchers(HttpMethod.GET, "/pokemones/**").authenticated()
			.antMatchers(HttpMethod.DELETE, "/pokemones/**").authenticated()
			.anyRequest().authenticated()
			.and()
			.csrf().disable();
	}
	
}
