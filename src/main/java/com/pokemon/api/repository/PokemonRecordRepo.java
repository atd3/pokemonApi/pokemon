package com.pokemon.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pokemon.api.entity.PokemonRecord;

public interface PokemonRecordRepo extends JpaRepository<PokemonRecord, Integer> {
	public Optional<PokemonRecord> findByUserId(int userId);
}
