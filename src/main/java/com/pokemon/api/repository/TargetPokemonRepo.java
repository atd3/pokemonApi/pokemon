package com.pokemon.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pokemon.api.entity.TargetPokemon;

public interface TargetPokemonRepo extends JpaRepository<TargetPokemon, Integer> {

}
