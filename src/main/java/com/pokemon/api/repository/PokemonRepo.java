package com.pokemon.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pokemon.api.entity.Pokemon;

public interface PokemonRepo extends JpaRepository<Pokemon, Integer> {

}
