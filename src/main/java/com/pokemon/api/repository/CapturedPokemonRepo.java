package com.pokemon.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pokemon.api.entity.CapturedPokemon;

public interface CapturedPokemonRepo extends JpaRepository<CapturedPokemon, Integer> {
	public Optional<CapturedPokemon> findByUserId(int userId);
}
