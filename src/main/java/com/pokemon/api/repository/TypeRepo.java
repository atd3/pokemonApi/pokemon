package com.pokemon.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pokemon.api.entity.Type;

public interface TypeRepo extends JpaRepository<Type, Integer> {

}
