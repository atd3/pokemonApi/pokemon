package com.pokemon.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pokemon.api.entity.User;

public interface UserRepo extends JpaRepository<User, Integer> {
	public Optional<User> findByUserName(String username);
}
