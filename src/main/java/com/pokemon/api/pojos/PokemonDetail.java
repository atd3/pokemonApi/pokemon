package com.pokemon.api.pojos;

import java.util.List;

public class PokemonDetail {
	private int id;
	private String name;
	private Sprites sprites;
	private List<TypeDesc> types;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Sprites getSprites() {
		return sprites;
	}
	public void setSprites(Sprites sprites) {
		this.sprites = sprites;
	}
	public List<TypeDesc> getTypes() {
		return types;
	}
	public void setTypes(List<TypeDesc> types) {
		this.types = types;
	}

}
