package com.pokemon.api.pojos;

public class TypeDesc {
	private int slot;
	private Descriptor type;
	public int getSlot() {
		return slot;
	}
	public void setSlot(int slot) {
		this.slot = slot;
	}
	public Descriptor getType() {
		return type;
	}
	public void setType(Descriptor type) {
		this.type = type;
	}

}
