package com.pokemon.api.pojos;

import com.pokemon.api.entity.Pokemon;
import com.pokemon.api.entity.PokemonRecord;

public class ResultadoBusqueda {
	private Pokemon pokemon;
	private PokemonRecord pokemonrecord;
	public Pokemon getPokemon() {
		return pokemon;
	}
	public void setPokemon(Pokemon pokemon) {
		this.pokemon = pokemon;
	}
	public PokemonRecord getPokemonrecord() {
		return pokemonrecord;
	}
	public void setPokemonrecord(PokemonRecord pokemonrecord) {
		this.pokemonrecord = pokemonrecord;
	}

}
