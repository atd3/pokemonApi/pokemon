package com.pokemon.api.service;

import com.pokemon.api.entity.User;
import com.pokemon.api.pojos.Resultado;

public interface UserService {
	
	public User insertUser(User user);
	public Resultado login(User user);
	public User getUser(String userName);

}
