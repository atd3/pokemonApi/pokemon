package com.pokemon.api.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.pokemon.api.entity.CapturedPokemon;
import com.pokemon.api.entity.Pokemon;
import com.pokemon.api.entity.PokemonRecord;
import com.pokemon.api.entity.TargetPokemon;
import com.pokemon.api.entity.Type;
import com.pokemon.api.pojos.PokemonDetail;
import com.pokemon.api.pojos.Pokemons;
import com.pokemon.api.pojos.ResultadoBusqueda;
import com.pokemon.api.repository.CapturedPokemonRepo;
import com.pokemon.api.repository.PokemonRecordRepo;
import com.pokemon.api.repository.PokemonRepo;
import com.pokemon.api.repository.TargetPokemonRepo;
import com.pokemon.api.repository.TypeRepo;
import com.pokemon.api.service.PokemonService;

@Service
public class PokemonServiceImpl implements PokemonService {

	@Autowired
	private PokemonRepo pokemonRepo; 

	@Autowired
	private TypeRepo typeRepo; 	

	@Autowired
	private PokemonRecordRepo pokemonRecordRepo; 	
	
	@Autowired
	private TargetPokemonRepo targetPokemonRepo; 	

	@Autowired
	private CapturedPokemonRepo capturedPokemonRepo; 	

	@Override
	public ResultadoBusqueda buscarPokemonEnApi() {
		ResultadoBusqueda resultado = new ResultadoBusqueda();
		RestTemplate plantilla = new RestTemplate();
		Pokemons pokemons = plantilla.getForObject("https://pokeapi.co/api/v2/pokemon/?offset=0&limit=50", Pokemons.class);
		int r =  (int) (Math.random() * 100);
		int randomIndex = r / 2;
		PokemonDetail pokemonDetail = plantilla.getForObject(pokemons.getResults().get(randomIndex - 1).getUrl(), PokemonDetail.class);
		
		PokemonRecord pokemonRecord = new PokemonRecord();
		Pokemon pokemon = pokemonRepo.findById(Integer.valueOf( pokemonDetail.getId() )).orElse(null);
		if(pokemon == null) {
			Type type = new Type();
			type.setName( pokemonDetail.getTypes().get(0).getType().getName() );
			type = typeRepo.save(type);
			
			pokemon = new Pokemon();
			pokemon.setId( Integer.valueOf( pokemonDetail.getId() ) );
			pokemon.setName( pokemonDetail.getName() );
			pokemon.setSprite( pokemonDetail.getSprites().getFront_default() );
			pokemon.setTypeId( type.getId() );
			pokemon = pokemonRepo.save(pokemon);
			
			pokemonRecord = new PokemonRecord();
			// TODO quitar valor fijo de user id
			pokemonRecord.setUserId(1);
			pokemonRecord.setPokemonId(pokemon.getId());
			pokemonRecord.setSeen(1);
			pokemonRecord.setCaptured(0);
			pokemonRecord.setSkiped(0);
			pokemonRecord = pokemonRecordRepo.save(pokemonRecord);
			
			TargetPokemon targetPokemon = new TargetPokemon();
			// TODO quitar valor fijo de user id
			targetPokemon.setUserId(1);
			targetPokemon.setPokemonId(pokemon.getId());
			targetPokemon.setAttack(0);
			targetPokemon.setDefense(0);
			targetPokemon = targetPokemonRepo.save(targetPokemon);
			
			resultado.setPokemon(pokemon);
			resultado.setPokemonrecord(pokemonRecord);
		}else {
			resultado.setPokemon(pokemon);
			
		}
		
		return resultado;
	}

	@Override
	public boolean escaparPokemon(Integer id) {
		boolean resultado = false;
		try {
			targetPokemonRepo.deleteById(id);
			resultado = true;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return resultado;
	}

	@Override
	public CapturedPokemon capturarPokemon(Pokemon pokemon, int idUser) {
		List<TargetPokemon> lst = targetPokemonRepo.findAll()
													.stream()
													.filter(elemento -> ( elemento.getUserId() == idUser && elemento.getPokemonId() == pokemon.getId() ) )
													.collect(Collectors.toList());

		CapturedPokemon capturedPokemon = null;
		if(lst.size() > 0) {
			capturedPokemon = new CapturedPokemon();
			capturedPokemon.setUserId( lst.get(0).getUserId() );
			capturedPokemon.setPokemonId( lst.get(0).getPokemonId() );
			capturedPokemon.setAttack( lst.get(0).getAttack() );
			capturedPokemon.setDefense( lst.get(0).getDefense() );
			capturedPokemon = capturedPokemonRepo.save(capturedPokemon);

			List<PokemonRecord> lstRec = pokemonRecordRepo.findAll()
					.stream()
					.filter(elemento -> ( elemento.getUserId() == idUser && elemento.getPokemonId() == pokemon.getId() ) )
					.collect(Collectors.toList());

			if(lstRec.size() > 0) {
				lstRec.get(0).setCaptured(1);
				pokemonRecordRepo.save( lstRec.get(0) );
			}
		}

		return capturedPokemon;
	}

	@Override
	public List<PokemonRecord> getPokemonRecordByUser(Integer idUser) {
		List<PokemonRecord> lst = pokemonRecordRepo.findAll()
									.stream()
									.filter(elemento -> elemento.getUserId() == idUser)
									.collect(Collectors.toList());
		return lst;
	}

	@Override
	public List<CapturedPokemon> getPokemonCapturedByUser(Integer idUser) {
		List<CapturedPokemon> lst = capturedPokemonRepo.findAll()
									.stream()
									.filter(elemento -> elemento.getUserId() == idUser)
									.collect(Collectors.toList());
		return lst;
	}

	@Override
	public boolean liberarPokemon(Integer id) {
		boolean resultado = false;
		try {
			capturedPokemonRepo.deleteById(id);
			resultado = true;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return resultado;
	}


	
}
