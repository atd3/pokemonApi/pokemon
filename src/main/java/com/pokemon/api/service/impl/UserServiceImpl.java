package com.pokemon.api.service.impl;


import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pokemon.api.entity.User;
import com.pokemon.api.pojos.Resultado;
import com.pokemon.api.repository.UserRepo;
import com.pokemon.api.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepo userRepo;
	
	@Override
	public User insertUser(User user) {
		user = userRepo.save(user);
		return user;
	}

	@Override
	public User getUser(String userName) {
		User user = userRepo.findByUserName(userName).orElse(null);
		return user;
	}

	@Override
	public Resultado login(User user) {
		Resultado resultado = new Resultado();
		List<User> lst = userRepo.findAll()
									.stream()
									.filter(e -> e.getUserName().equals( user.getUserName() ) && e.getPassword().equals( user.getPassword() ))
									.collect(Collectors.toList());

		resultado.setResultado(lst.size() > 0);
		resultado.setMensaje(lst.size() > 0 ? "Autenticación Ok" : "Error en credenciales");
		return resultado;
	}

}
