package com.pokemon.api.service;

import java.util.List;

import com.pokemon.api.entity.CapturedPokemon;
import com.pokemon.api.entity.Pokemon;
import com.pokemon.api.entity.PokemonRecord;
import com.pokemon.api.pojos.ResultadoBusqueda;

public interface PokemonService {

	public ResultadoBusqueda buscarPokemonEnApi();
	public boolean escaparPokemon(Integer id);
	public CapturedPokemon capturarPokemon(Pokemon pokemon, int idUser);
	public List<PokemonRecord> getPokemonRecordByUser(Integer idUser);
	public List<CapturedPokemon> getPokemonCapturedByUser(Integer idUser);
	public boolean liberarPokemon(Integer id);
}
